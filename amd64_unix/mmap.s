;; Copyright (c) 2021 Andrew Apted
;; Use of this code is governed by an MIT-style license.
;; See the accompanying "LICENSE.md" file for the full text.

section .text

%define sys_mmap    9
%define sys_munmap  11

%define PROT_READ   1
%define PROT_WRITE  2

%define MAP_PRIVATE    0x02
%define MAP_ANONYMOUS  0x20

global __ros_alloc_pages
global __ros_free_pages

__ros_alloc_pages:
	; RDI = total size

	push	rbp
	mov	rbp,rsp

	mov	rsi,rdi				; size
	xor	rdi,rdi				; address
	mov	edx,PROT_READ|PROT_WRITE	; prot
	mov	r10d,MAP_PRIVATE|MAP_ANONYMOUS	; flags
	mov	r8,-1				; fd
	xor	r9,r9				; offset

	mov	eax,sys_mmap
	syscall

	leave
	ret


__ros_free_pages:
	; RDI = pointer to first page
	; RSI = total size

	push	rbp
	mov	rbp,rsp

	mov	eax,sys_munmap
	syscall

	leave
	ret
