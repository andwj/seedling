;; Copyright (c) 2021 Andrew Apted
;; Use of this code is governed by an MIT-style license.
;; See the accompanying "LICENSE.md" file for the full text.

section .text

%define sys_read	0
%define sys_write	1

%define STDIN		0
%define STDOUT		1
%define STDERR		2

global __ros_input_byte
global __ros_print_byte
global __ros_error_byte

__ros_input_byte:
	push	rbp
	mov	rbp,rsp

	; use the stack as our buffer
	sub	rsp,16

	mov	edi,STDIN     ; fd
	lea	rsi,[rbp-4]   ; buf
	mov	edx,1         ; count

	mov	eax,sys_read
	syscall

	test	rax,rax
	jz	.eof
	js	.error

	xor	rax,rax
	mov	al,[rbp-4]

	leave
	ret
.eof:
	mov	rax,dword -1
	leave
	ret
.error:
	mov	rax,dword -2
	leave
	ret


__ros_print_byte:
	; RDI = raw byte

	push	rbp
	mov	rbp,rsp

	; use the stack as our buffer
	sub	rsp,16
	mov	[rbp-4],dil

	mov	edi,STDOUT    ; fd
	lea	rsi,[rbp-4]   ; buf
	mov	edx,1         ; count

	mov	eax,sys_write
	syscall

	leave
	ret


__ros_error_byte:
	; RDI = raw byte

	push	rbp
	mov	rbp,rsp

	; use the stack as our buffer
	sub	rsp,16
	mov	[rbp-4],dil

	mov	edi,STDERR    ; fd
	lea	rsi,[rbp-4]   ; buf
	mov	edx,1         ; count

	mov	eax,sys_write
	syscall

	leave
	ret
