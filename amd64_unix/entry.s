;; Copyright (c) 2021 Andrew Apted
;; Use of this code is governed by an MIT-style license.
;; See the accompanying "LICENSE.md" file for the full text.

section .text

%define sys_exit  60

extern main

global _start
global __exit

_start:
	; TODO handle arguments and environment vars

	xor	rbp,rbp
	call	main

	mov	edi,0

__exit:
	; RDI = status code (32 bits)

	; this will not return!
	mov	eax,sys_exit
	syscall

	hlt
