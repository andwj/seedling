
SEEDLING
========

by Andrew Apted, 2022.


About
-----

Seedling is a standard library for use with my Rowan programming language.
It is written it Rowan and (where needed) assembly language.


Legalese
--------

Seedling is under a permissive MIT license.

Seedling comes with NO WARRANTY of any kind, express or implied.

See the [LICENSE.md](LICENSE.md) document for the full terms.


Links
-----

https://gitlab.com/andwj/rowan

