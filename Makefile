#
#  Requires GNU make.
#

ROWAN=rowan
SAGELEAF=sageleaf
NASM=nasm -f elf64 -g

AR=ar rcv
RANLIB=ranlib

BUILD_DIR=_build

DUMMY=$(BUILD_DIR)/zzdummy


#----- Object files --------------------------------------------

all: $(DUMMY) $(BUILD_DIR)/lib.a

OBJS =	$(BUILD_DIR)/rand.o

ARCH_OBJS = \
	\
	$(BUILD_DIR)/os/entry_amd64_unix.o   \
	$(BUILD_DIR)/os/term_amd64_unix.o    \
	\
	$(BUILD_DIR)/mem/pages_amd64_unix.o


#---- Rules ----------------------------------------------------

CODE =	rand/random.rx

LLS =	$(BUILD_DIR)/rand.ll

$(LLS) &: $(CODE) Build.list
	$(ROWAN) Build.list

$(BUILD_DIR)/%.s: $(BUILD_DIR)/%.ll
	$(SAGELEAF) $< -o $@

$(BUILD_DIR)/%.o: $(BUILD_DIR)/%.s
	$(NASM) $< -o $@

## $(BUILD_DIR)/os/%.o: os/%.s
## 	$(NASM) $< -o $@

## $(BUILD_DIR)/mem/%.o: mem/%.s
## 	$(NASM) $< -o $@


#----- Targets -------------------------------------------------

$(BUILD_DIR)/lib.a: $(OBJS)
	$(AR) $@ $^
	$(RANLIB) $@

clean:
	rm -f $(BUILD_DIR)/*.*
	rm -f $(BUILD_DIR)/*/*.*

# this is used to create the build directory
$(DUMMY):
	mkdir -p $(BUILD_DIR)
	@touch $@

.PHONY: all clean

#--- editor settings ------------
# vi:ts=8:sw=8:noexpandtab
